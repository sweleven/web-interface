<div align="center" style="font-size: 7em">

<img src="https://img.icons8.com/emoji/192/000000/laptop-emoji.png"/></div>

<div align="center">
<h1>Web interface</h1>
</div>

<p align="center">
<a href="https://www.npmjs.com/@angular/core">
    <img src="https://img.shields.io/npm/v/@angular/core.svg?logo=npm&logoColor=fff&label=NPM+package&color=limegreen" alt="Angular on npm" />
  </a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_web-interface"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_web-interface&metric=alert_status" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_web-interface"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_web-interface&metric=sqale_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_web-interface"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_web-interface&metric=security_rating" alt="quality gate"></a>
<a href="https://sonarcloud.io/dashboard?id=sweleven_web-interface"  target="_blank"><img src="https://sonarcloud.io/api/project_badges/measure?project=sweleven_web-interface&metric=reliability_rating" alt="quality gate"></a>
<a href="https://gitlab.com/sweleven/web-interface/-/pipelines"><img src="https://gitlab.com/sweleven/web-interface/badges/master/pipeline.svg" /></a>
</p>

<hr>

## Description

Web interface of [BlockCOVID](https://sweleven.gitlab.io/blockcovid/) built with <a href="https://angular.io/">Angular</a>.

## Docs
- [Docs](https://sweleven.gitlab.io/web-interface)
- [Developer manual](https://sweleven.gitlab.io/blockcovid/docs/manuale-sviluppatore/web-app/descrizione/)
- [User manual](https://sweleven.gitlab.io/blockcovid/docs/manuale-utente/applicazione_web/)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Filippo Pinton](https://gitlab.com/Butterneck.com)
- Website - [https://sweleven.gitlab.io](https://sweleven.gitlab.io/)

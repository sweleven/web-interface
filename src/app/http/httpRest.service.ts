// import { Observable } from 'rxjs';
// import { share } from 'rxjs/operators';
// import { PaginatedIndex } from 'app/main/model/paginated-index.interface';
// import { HttpService } from './http.service';
// import { HttpClient } from '@angular/common/http';

// export abstract class HttpRestService<T> extends HttpService
// {
//     constructor(
//         http: HttpClient,
//         contextUrl: string
//     )
//     {
//         super(http, contextUrl);
//     }

//     index(params: any = {}) : Observable<T[]>
//     {
//         return this.get<T[]>(params).pipe(share());
//     }

//     indexPaginated(
//         params: any //TODO: substitute with typed generic
//     ): Observable<PaginatedIndex<T>> {
//         return this.get(params);
//     }


//     add(task:T) : Observable<T>
//     {
//         return this.post<T>(task).pipe(share());
//     }

//     view(id: number): Observable<T> {
//         return super.post<T>('/view', { id });
//     }

//     save(task:T) : Observable<T>
//     {
//         return this.edit(task);
//     }

//     edit(task:T) : Observable<T>
//     {
//         return this.post<T>('/edit', task).pipe(share());
//     }

//     delete(task:T) : Observable<any>
//     {
//         return this.post('/delete', task).pipe(share());
//     }

//     clone(task: T): Observable<any> {
//         return this.post('/clone', task).pipe(share());
//     }
// }

import { of as observableOf,  Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Contacts, RecentUsers, UserData } from '../data/users';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService extends UserData {

  constructor(private readonly http: HttpClient) {
    super();
  }

  getUser(): Observable<any> {
    return this.http.get(`/api/identities/profile`);
  }

  getContacts(): Observable<Contacts[]> {
    return of(null);
  }

  getRecentUsers(): Observable<RecentUsers[]> {
    return of(null);
  }
}

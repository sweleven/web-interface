import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { timer } from 'rxjs';
import { debounce, filter, switchMap, tap } from 'rxjs/operators';
import { DialogUserPromptComponent } from './dialog-user-prompt/dialog-user-prompt.component';
import { User } from './models/user.model';
import { UsersService } from './services/users.service';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  public users: User[] = [];

  public searchForm = this.fb.group({
    searchQuery: '',
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersService: UsersService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ users }) => {
          this.users = users;
        }),
      )
      .subscribe();
  }

  ngOnInit(): void {

    // Update users array according to live search (debounce 300ms)
    this.searchForm.valueChanges.pipe(
      debounce(() => timer(300)),
      tap((values) => {
        this.onSearchUser(values);
      }),
    ).subscribe();
  }

  onSearchUser(values): void {
    this.usersService.searchUser(values).pipe(
      tap((users: User[]) => {
        this.users = users;
      }),
    ).subscribe();
  }

  addUser() {
    this.dialogService.open(DialogUserPromptComponent)
      .onClose
      .pipe(
        filter((status: boolean) => status),
        switchMap(() => {
          return this.usersService.getUsers();
        }),
        tap((users: User[]) => {
          this.users = users;
        }),
      )
      .subscribe();
  }
}

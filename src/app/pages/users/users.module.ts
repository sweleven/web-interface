import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbInputModule, NbCardModule, NbListModule, NbButtonModule,
  NbAccordionModule, NbUserModule, NbDialogModule, NbStepperModule, NbToggleModule,
  NbCheckboxModule, NbRadioModule, NbAlertModule, NbIconModule,
} from '@nebular/theme';
import { DialogUserPromptComponent } from './dialog-user-prompt/dialog-user-prompt.component';
import { UsersResolver } from './resolvers/users.resolver';
import { UsersService } from './services/users.service';
import { UserDetailsResolver } from './user-details/resolvers/user-details.resolver';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserHistoryDownloadDialogComponent } from './user-details/user-history-download-dialog/user-history-download-dialog.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';

@NgModule({
  declarations: [
    UsersComponent,
    UserDetailsComponent,
    DialogUserPromptComponent,
    UserHistoryDownloadDialogComponent,
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    NbInputModule,
    NbCardModule,
    NbListModule,
    NbButtonModule,
    NbAccordionModule,
    NbUserModule,
    NbDialogModule.forRoot(),
    NbStepperModule,
    NbToggleModule,
    NbCheckboxModule,
    NbRadioModule,
    NbAlertModule,
    NbIconModule,
  ],
  providers: [
    UsersService,
    UsersResolver,
    UserDetailsResolver,
  ],
  entryComponents: [
    UserHistoryDownloadDialogComponent,
  ]
})
export class UsersModule { }

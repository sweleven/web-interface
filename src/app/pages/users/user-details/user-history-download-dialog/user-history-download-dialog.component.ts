import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'user-history-download-dialog',
  templateUrl: './user-history-download-dialog.component.html',
  styleUrls: ['./user-history-download-dialog.component.scss']
})
export class UserHistoryDownloadDialogComponent implements OnInit {

  @Input() history: any;

  @Input() usersService: UsersService;

  public downloadForm: FormGroup;

  constructor(
    protected ref: NbDialogRef<UserHistoryDownloadDialogComponent>,
    private readonly fb: FormBuilder,
  ) {
    this.downloadForm = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    })
  }


  ngOnInit(): void {
    console.log(this.history);
  }

  onCheckboxChange(checked, value) {
    const checkArray: FormArray = this.downloadForm.get('checkArray') as FormArray;

    if (checked) {
      checkArray.push(new FormControl(value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  public download() {
    console.log(this.downloadForm.value.checkArray.includes('cleanings'));
    if (this.downloadForm.value.checkArray.includes('cleanings')) {
      this.usersService.downloadHistory(this.history.cleanings_history, 'cleanings');
    } 

    console.log(this.downloadForm.value.checkArray.includes('bookings'));
    if (this.downloadForm.value.checkArray.includes('bookings')) {
      this.usersService.downloadHistory(this.history.bookings_history, 'bookings');
    }

    this.ref.close();
  }

}

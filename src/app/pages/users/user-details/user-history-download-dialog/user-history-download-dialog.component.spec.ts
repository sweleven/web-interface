import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHistoryDownloadDialogComponent } from './user-history-download-dialog.component';

describe('DownloadDialogComponent', () => {
  let component: UserHistoryDownloadDialogComponent;
  let fixture: ComponentFixture<UserHistoryDownloadDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHistoryDownloadDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHistoryDownloadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

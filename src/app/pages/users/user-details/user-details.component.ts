import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDeletionComponent } from 'app/pages/miscellaneous/confirm-deletion/confirm-deletion.component';
import { of } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import SHA3 from 'sha3';
import { User } from '../models/user.model';
import { UsersService } from '../services/users.service';
import { UserHistoryDownloadDialogComponent } from './user-history-download-dialog/user-history-download-dialog.component';

@Component({
  selector: 'ngx-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent implements OnInit {

  public user: User;
  public history: any;

  public edit: boolean = false;

  public activationForm = new FormControl(true);

  public editUserForm = this.fb.group({
    email: [
      '',
      [
        Validators.email,
      ],
    ],
    firstName: [
      '',
      [
        Validators.required,
      ],
    ],
    lastName: [
      '',
      [
        Validators.required,
      ],
    ],
    role: [
      '',
      [
        Validators.required,
      ],
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly usersService: UsersService,
    private readonly dialogService: NbDialogService,
    private readonly router: Router,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ user }) => this.user = user),
        tap(() => {
          this.editUserForm.setValue({
            email: this.user.email,
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            role: this.user.roles[0]?.name,
          });
        }),

        // TODO: for user activation
        // tap(({ user }) => {
        //   this.activationForm.setValue(user.enabled)
        //   this.activationForm.valueChanges.pipe(
        //     switchMap((val) => {
        //       return this.usersService.updateUser(this.user, { enabled: val });
        //     }),
        //   ).subscribe();
        // }),
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.usersService.getUserHistory(this.user.email).pipe(
      tap((history: any) => this.history = history),
      tap(() => console.log(this.history)),
    ).subscribe();
  }

  public deleteUser() {
    this.dialogService.open(ConfirmDeletionComponent)
      .onClose.pipe(
        filter((confirm) => confirm),
        switchMap(() => this.usersService.deleteUser(this.user)),
        switchMap(() => {
          return this.router.navigate(['/pages/users']);
        }),
      ).subscribe();
  }

  public saveUser() {
    this.usersService.updateUser(this.user.id, this.editUserForm.value).subscribe((user: User) => {
      this.user = user;
      this.editUserForm.markAsPristine();
    });
  }

  public downloadHistory() {
    this.dialogService.open(UserHistoryDownloadDialogComponent, {
      context: {
        history: this.history,
        usersService: this.usersService,
      },
    })
  }

  public getTransactionUrl(historyItem) {
    const tmp = { ...historyItem };
    delete tmp['type'];
    delete tmp['tx_url'];

    console.log(tmp);
    const hash = new SHA3().update(JSON.stringify(tmp)).digest('hex');
    this.usersService.getHistoryItemTransaction(hash).pipe(
      tap((tx_info: any) => {
        console.log(tx_info);
        historyItem.tx_url = tx_info.etherscanUrl;
      }),
    ).subscribe();
  }
}

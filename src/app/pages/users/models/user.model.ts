export class User {
    id?: string;
    createdTimestamp?: Date;
    username: string;
    enabled?: string;
    totp?: string;
    emailVerified?: string;
    firstName?: string;
    lastName?: string;
    email: string;
    attributes: any;
    disabledCredentialTypes?: any;
    requiredActions?: any;
    notBefore?: any;
    access?: any;
    roles: any;
    role?: string;
  }


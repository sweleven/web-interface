import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpService } from 'app/http/http.service';
import { Booking } from 'app/pages/miscellaneous/models/booking.model';
import { Cleaning } from 'app/pages/miscellaneous/models/cleaning.model';
import { combineLatest, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { User } from '../models/user.model';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root',
})
export class UsersService extends HttpService<User> {

  constructor(
    protected readonly http: HttpClient,
  ) {
    super(http, 'identities/users');
  }

  public getUsers(): Observable<User[]> {
    return this.get();
  }

  public isUserEmailTaken(userEmail: string): Observable<boolean> {
    return this.searchUser({ searchQuery: userEmail }).pipe(
      map((users: User[]) => users.length !== 0),
    );
  }

  public addUser(user: User): Observable<User> {
    return this.post(user);
  }

  public searchUser(searchParams: any): Observable<User[]> {

    const params = new URLSearchParams(searchParams);
    return this.get(params.toString());

  }

  public getUser(id: string): Observable<User> {
    return this.http.get<User>(`/api/identities/users/${id}`);
  }

  public updateUser(userId: string, values: any): Observable<User> {
    return this.http.patch<User>(`/api/identities/users/${userId}`, values);
  }

  public deleteUser(user: User): Observable<void> {
    return this.http.delete(`/api/identities/users/${user.id}`).pipe(
      map(() => null),
    );
  }

  public getUserHistory(userId: string): Observable<any> {
    const bookingsHistory: Observable<Booking[]> = this.http.get<Booking[]>(`/api/booking/history/user/${userId}`);
    const cleaningsHistory: Observable<Cleaning[]> = this.http.get<Cleaning[]>(`/api/cleanings/history/user/${userId}`);

    return combineLatest([bookingsHistory, cleaningsHistory]).pipe(
      map(([bookingsHistory, cleaningsHistory]) => {

        const toBeReturned = {
          'bookings_history': [...bookingsHistory],
          'cleanings_history': [...cleaningsHistory],
          'merged_history': [],
        };
        let currentLast = null;

        while (bookingsHistory.length || cleaningsHistory.length) {

          if (bookingsHistory.length && cleaningsHistory.length) {

            if ((bookingsHistory[0].checkout_datetime && bookingsHistory[0].checkout_datetime < cleaningsHistory[0].datetime) || bookingsHistory[0].end_datetime < cleaningsHistory[0].datetime) {
              currentLast = bookingsHistory.shift();
              currentLast['type'] = 'booking';
            } else {
              currentLast = cleaningsHistory.shift();
              currentLast['type'] = 'cleaning';
            }

          } else if (bookingsHistory.length) {
            currentLast = bookingsHistory.shift();
            currentLast['type'] = 'booking';
          } else {
            currentLast = cleaningsHistory.shift();
            currentLast['type'] = 'cleaning';
          }

          toBeReturned.merged_history.unshift(currentLast);
        }

        return toBeReturned;
      }),
    );

  }

  public downloadHistory(history: any, filename) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(history[0]);
    let csv = history.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var blob = new Blob([csvArray], { type: 'text/csv' })
    saveAs(blob, `${filename}.csv`);
  }


  public getHistoryItemTransaction(hash: string) {
    return this.http.get(`/api/blockchain-interactor/blocks/hash/${hash}`);
  }

}

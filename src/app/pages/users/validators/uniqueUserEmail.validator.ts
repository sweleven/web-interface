import { Injectable } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { UsersService } from '../services/users.service';

@Injectable({ providedIn: 'root' })
export class UniqueUserEmailValidator implements AsyncValidator {
  constructor(private usersService: UsersService) {}

  validate(
    ctrl: AbstractControl,
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.usersService.isUserEmailTaken(ctrl.value).pipe(
      map(isTaken => (isTaken ? { uniqueUserEmail: true } : null)),
      catchError(() => of(null)),
    );
  }
}

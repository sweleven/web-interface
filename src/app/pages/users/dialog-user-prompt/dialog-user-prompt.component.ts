import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NbDialogRef, NbStepperComponent } from '@nebular/theme';
import { catchError, tap } from 'rxjs/operators';
import { UsersService } from '../services/users.service';
import { UniqueUserEmailValidator } from '../validators/uniqueUserEmail.validator';
import { User } from '../models/user.model';

export type CreationStatus = 'creating' | 'failed' | 'successful';
@Component({
  selector: 'ngx-dialog-user-prompt',
  templateUrl: 'dialog-user-prompt.component.html',
  styleUrls: [
    'dialog-user-prompt.component.scss',
    'spinner.scss',
  ],
})
export class DialogUserPromptComponent {

  @ViewChild('stepper') stepper: NbStepperComponent;

  public creationResult: CreationStatus = 'creating';

  public userEmailForm = this.fb.group({
    email: [
      '',
      [
        Validators.required,
        Validators.email,
      ],
      [
        new UniqueUserEmailValidator(this.usersService),
      ],
    ],
  });

  public userRoleForm = this.fb.group({
    role: [
      '',
      [
        Validators.required,
      ],
    ],
  });

  public user = new User();

  constructor(
    protected ref: NbDialogRef<DialogUserPromptComponent>,
    private readonly fb: FormBuilder,
    private readonly usersService: UsersService,
  ) { }

  cancel() {
    this.ref.close(false);
  }

  close() {
    this.ref.close(true);
  }

  onEmailSubmit() {
    this.userEmailForm.markAsDirty();
    this.user.email = this.userEmailForm.value.email;
    this.stepper.next();
  }

  onRoleSubmit() {
    this.userRoleForm.markAsDirty();
    this.user.role = this.userRoleForm.value.role;
    this.stepper.next();
  }

  onAddUser() {
    this.creationResult = 'creating';
    return this.usersService.addUser(this.user).pipe(
      tap((user) => this.creationResult = 'successful'),
      catchError((err, caught) => {
        console.log(err); // tslint:disable-line no-console
        this.creationResult = 'failed';
        return null;
      }),
    ).subscribe();
  }

  getCreationResult() {
    return this.creationResult.toString();
  }

}

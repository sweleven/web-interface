import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersResolver } from './resolvers/users.resolver';
import { UserDetailsResolver } from './user-details/resolvers/user-details.resolver';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UsersComponent } from './users.component';

const routes: Routes = [{
    path: '',
    component: UsersComponent,
    resolve: {
        users: UsersResolver,
    },
},
{
    path: ':id',
    component: UserDetailsComponent,
    resolve: {
        user: UserDetailsResolver,
    },
},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsersRoutingModule {

}

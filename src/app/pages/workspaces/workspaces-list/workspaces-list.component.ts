import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDeletionComponent } from 'app/pages/miscellaneous/confirm-deletion/confirm-deletion.component';
import { timer } from 'rxjs';
import { debounce, filter, map, switchMap, tap } from 'rxjs/operators';
import { DialogWorkspacePromptComponent } from '../dialog-workspace-prompt/dialog-workspace-prompt.component';
import { Workspace } from '../models/workspace.model';
import { WorkspacesService } from '../services/workspaces.service';

@Component({
  selector: 'ngx-workspaces-list',
  templateUrl: './workspaces-list.component.html',
  styleUrls: ['./workspaces-list.component.scss'],
})
export class WorkspacesListComponent implements OnInit {

  @Input()
  public workspaces: Workspace[];

  public searchForm = this.fb.group({
    name: '',
  });

  constructor(
    private readonly fb: FormBuilder,
    private dialogService: NbDialogService,
    private readonly workspacesService: WorkspacesService,
  ) { }

  ngOnInit(): void {
    // Update workspaces array according to live search (debounce 300ms)
    this.searchForm.valueChanges.pipe(
      debounce(() => timer(300)),
      tap((values) => {
        this.onSearchWorkspace(values);
      }),
    ).subscribe();
  }

  onSearchWorkspace(values): void {
    this.workspacesService.searchWorkspace(values).pipe(
      tap((workspaces: Workspace[]) => {
        this.workspaces = workspaces;
      }),
    ).subscribe();
  }

  addWorkspace() {
    this.dialogService.open(DialogWorkspacePromptComponent)
      .onClose
      .pipe(
        filter((status: boolean) => status),
        switchMap(() => {
          return this.workspacesService.getWorkspaces();
        }),
        tap((workspaces: Workspace[]) => {
          this.workspaces = workspaces;
        }),
      )
      .subscribe();
  }

  deleteWorkspace(workspace) {
    this.dialogService.open(ConfirmDeletionComponent)
      .onClose.pipe(
        filter((confirm) => confirm),
        switchMap(() => this.workspacesService.deleteWorkspace(workspace)),
        map((_workspace: Workspace) => {
          this.workspaces = this.workspaces.filter(__workspace => __workspace._id !== _workspace._id);
        }),
      ).subscribe();
  }

}

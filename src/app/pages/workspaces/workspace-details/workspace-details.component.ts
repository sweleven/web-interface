import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { switchMap, tap } from 'rxjs/operators';
import SHA3 from 'sha3';
import { Workspace } from '../models/workspace.model';
import { WorkspacesService } from '../services/workspaces.service';
import { WorkspaceHistoryDownloadDialogComponent } from './workspace-history-download-dialog/workspace-history-download-dialog.component';

@Component({
  selector: 'ngx-workspace-details',
  templateUrl: './workspace-details.component.html',
  styleUrls: ['./workspace-details.component.scss'],
})
export class WorkspaceDetailsComponent implements OnInit {

  public workspace: Workspace;
  public history: any;

  public edit: boolean = false;

  public activationForm = new FormControl(true);

  public editWorkspaceForm = this.fb.group({
    name: [
      '',
      [
        Validators.required,
      ],
    ],
    rfid: [
      '',
      [
        Validators.required,
      ],
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly workspacesService: WorkspacesService,
    private readonly dialogService: NbDialogService,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ workspace }) => this.workspace = workspace),
        tap(() => {
          this.editWorkspaceForm.setValue({
            name: this.workspace.name,
            rfid: this.workspace.rfid,
          });
        }),

        // TODO: for workspace activation
        tap(({ workspace }) => {
          this.activationForm.setValue(workspace.enabled)
          this.activationForm.valueChanges.pipe(
            switchMap((val) => {
              return this.workspacesService.updateWorkspace(this.workspace._id, { enabled: val });
            }),
            tap((_workspace: Workspace) => this.workspace = _workspace),
          ).subscribe();
        }),
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.workspacesService.getWorkspaceHistory(this.workspace._id).pipe(
      tap((history: any) => this.history = history),
      tap(() => console.log(this.history)),
    ).subscribe();
  }

  public saveWorkspace() {
    this.workspacesService.updateWorkspace(this.workspace._id, this.editWorkspaceForm.value).subscribe((workspace: Workspace) => {
      this.workspace = workspace;
      this.editWorkspaceForm.markAsPristine();
    });
  }

  public downloadHistory() {
    this.dialogService.open(WorkspaceHistoryDownloadDialogComponent, {
      context: {
        history: this.history,
        workspacesService: this.workspacesService,
      },
    })
  }

  public getTransactionUrl(historyItem) {
    const tmp = { ...historyItem };
    delete tmp['type'];
    delete tmp['tx_url'];

    console.log(tmp);
    const hash = new SHA3().update(JSON.stringify(tmp)).digest('hex');
    this.workspacesService.getHistoryItemTransaction(hash).pipe(
      tap((tx_info: any) => {
        console.log(tx_info);
        historyItem.tx_url = tx_info.etherscanUrl;
      }),
    ).subscribe();
  }
}

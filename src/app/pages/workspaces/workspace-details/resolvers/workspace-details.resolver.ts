import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Workspace } from '../../models/workspace.model';
import { WorkspacesService } from '../../services/workspaces.service';

@Injectable()
export class WorkspaceDetailsResolver implements Resolve<Workspace> {

    constructor(private roomsService: WorkspacesService) { }

    resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Workspace> | Promise<Workspace> | Workspace {
        return this.roomsService.getWorkspace(route.params.id);
    }
}

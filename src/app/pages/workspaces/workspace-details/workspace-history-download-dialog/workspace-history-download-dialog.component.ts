import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { WorkspacesService } from '../../services/workspaces.service';

@Component({
  selector: 'workspace-history-download-dialog',
  templateUrl: './workspace-history-download-dialog.component.html',
  styleUrls: ['./workspace-history-download-dialog.component.scss']
})
export class WorkspaceHistoryDownloadDialogComponent implements OnInit {

  @Input() history: any;

  @Input() workspacesService: WorkspacesService;

  public downloadForm: FormGroup;

  constructor(
    protected ref: NbDialogRef<WorkspaceHistoryDownloadDialogComponent>,
    private readonly fb: FormBuilder,
  ) {
    this.downloadForm = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    })
  }


  ngOnInit(): void {
    console.log(this.history);
  }

  onCheckboxChange(checked, value) {
    const checkArray: FormArray = this.downloadForm.get('checkArray') as FormArray;

    if (checked) {
      checkArray.push(new FormControl(value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  public download() {
    console.log(this.downloadForm.value.checkArray.includes('cleanings'));
    if (this.downloadForm.value.checkArray.includes('cleanings')) {
      console.log('ihhhhh');
      console.log(this.history);
      console.log(this.history.cleanings_history);
      this.workspacesService.downloadHistory(this.history.cleanings_history, 'cleanings');
    } 

    console.log(this.downloadForm.value.checkArray.includes('bookings'));
    if (this.downloadForm.value.checkArray.includes('bookings')) {
      console.log('ohghhh');
      console.log(this.history.bookings_history);
      this.workspacesService.downloadHistory(this.history.bookings_history, 'bookings');
    }

    this.ref.close();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceHistoryDownloadDialogComponent } from './workspace-history-download-dialog.component';

describe('DownloadDialogComponent', () => {
  let component: WorkspaceHistoryDownloadDialogComponent;
  let fixture: ComponentFixture<WorkspaceHistoryDownloadDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceHistoryDownloadDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceHistoryDownloadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

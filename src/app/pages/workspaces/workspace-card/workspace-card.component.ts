import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Workspace } from '../models/workspace.model';
import { WorkspacesService } from '../services/workspaces.service';
import { UniqueWorkspaceNameValidator } from '../validators/unique-workspace-name.validator';
import { UniqueWorkspaceRfidValidator } from '../validators/unique-workspace-rfid.validator';

@Component({
  selector: 'ngx-workspace-card',
  templateUrl: './workspace-card.component.html',
  styleUrls: ['./workspace-card.component.scss'],
})
export class WorkspaceCardComponent implements OnInit {

  public edit: boolean = false;

  public editForm = this.fb.group({
    rfid: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
      ],
      [
        new UniqueWorkspaceRfidValidator(this.workspacesService),
      ],
    ],
    name: [
      '',
      [
        Validators.required,
        Validators.minLength(4),
      ],
      [
        new UniqueWorkspaceNameValidator(this.workspacesService),
      ],
    ],
  });

  @Input()
  workspace: Workspace;

  @Output()
  onDelete = new EventEmitter<Workspace>();

  constructor(
    private readonly fb: FormBuilder,
    private readonly workspacesService: WorkspacesService,
  ) { }

  ngOnInit(): void {
  }

  public deleteWorkspace() {
    this.onDelete.emit(this.workspace);
  }

  public editWorkspace() {
    this.edit = true;
    this.editForm.setValue({
      name: this.workspace.name,
      rfid: this.workspace.rfid,
    });

  }

  public saveEdits() {
    // console.log(this.editForm.valid);
    this.edit = false;
  }

  public cancelEdits() {
    this.edit = false;
  }
}

import { Injectable } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { WorkspacesService } from '../services/workspaces.service';

@Injectable({ providedIn: 'root' })
export class UniqueWorkspaceRfidValidator implements AsyncValidator {
  constructor(private workspacesService: WorkspacesService) {}

  validate(
    ctrl: AbstractControl,
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.workspacesService.isRfidTaken(ctrl.value).pipe(
      map(isTaken => (isTaken ? { uniqueRfid: true } : null)),
      catchError(() => of(null)),
    );
  }
}

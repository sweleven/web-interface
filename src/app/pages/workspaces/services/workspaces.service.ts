import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'app/http/http.service';
import { Booking } from 'app/pages/miscellaneous/models/booking.model';
import { Cleaning } from 'app/pages/miscellaneous/models/cleaning.model';
import { combineLatest, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Workspace } from '../models/workspace.model';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root',
})
export class WorkspacesService extends HttpService<Workspace> {

  constructor(
    protected readonly http: HttpClient,
    private router: Router,
  ) {
    super(http, 'inventory/workspaces');
  }

  public getWorkspaces(roomId = ''): Observable<Workspace[]> {
    if (roomId === '') {
      roomId = this._getCurrentRoomId();
    }

    const params = new URLSearchParams({ room: roomId });
    return this.get(params.toString());
  }

  public getWorkspace(id: string): Observable<Workspace> {
    return this.http.get<Workspace>(`/api/inventory/workspaces/${id}`);
  }

  public isWorkspaceNameTaken(workspaceName: string, roomId = ''): Observable<boolean> {

    if (roomId === '') {
      roomId = this._getCurrentRoomId();
    }

    return this.searchWorkspace({ name: workspaceName, room: roomId }).pipe(
      map((workspaces: Workspace[]) => workspaces.length !== 0),
    );
  }

  public addWorkspace(workspace: Workspace): Observable<Workspace> {

    if (!!!workspace.room) {
      workspace.room = this._getCurrentRoomId();
    }

    return this.post(workspace);
  }

  public searchWorkspace(searchParams: any): Observable<Workspace[]> {

    if (!searchParams['room']) {
      searchParams['room'] = this._getCurrentRoomId();
    }

    const params = new URLSearchParams(searchParams);
    return this.get(params.toString());
  }

  private _getCurrentRoomId(): string {
    const re = new RegExp('[^\/]+(?=\/$|$)');

    return this.router.url.match(re)[0];
  }

  public isRfidTaken(rfid: string): Observable<boolean> {
    return this.http.get(`/api/inventory/rfid/${rfid}`).pipe(
      map((val) => !!val),
    );
  }

  public deleteWorkspace(workspace: Workspace): Observable<Workspace> {
    return this.delete(`/${workspace._id}`);
  }

  public updateWorkspace(workspaceId: string, values: any): Observable<Workspace> {
    return this.http.patch<Workspace>(`/api/inventory/workspaces/${workspaceId}`, values);
  }

  public getWorkspaceHistory(workspaceId: string): Observable<any> {
    const bookingsHistory: Observable<Booking[]> = this.http.get<Booking[]>(`/api/booking/history/workspace/${workspaceId}`);
    const cleaningsHistory: Observable<Cleaning[]> = this.http.get<Cleaning[]>(`/api/cleanings/history/workspace/${workspaceId}`);

    return combineLatest([bookingsHistory, cleaningsHistory]).pipe(
      map(([bookingsHistory, cleaningsHistory]) => {

        const toBeReturned = {
          'bookings_history': [...bookingsHistory],
          'cleanings_history': [...cleaningsHistory],
          'merged_history': [],
        };
        let currentLast = null;

        while (bookingsHistory.length || cleaningsHistory.length) {

          if (bookingsHistory.length && cleaningsHistory.length) {

            if ((bookingsHistory[0].checkout_datetime && bookingsHistory[0].checkout_datetime < cleaningsHistory[0].datetime) || bookingsHistory[0].end_datetime < cleaningsHistory[0].datetime) {
              currentLast = bookingsHistory.shift();
              currentLast['type'] = 'booking';
            } else {
              currentLast = cleaningsHistory.shift();
              currentLast['type'] = 'cleaning';
            }

          } else if (bookingsHistory.length) {
            currentLast = bookingsHistory.shift();
            currentLast['type'] = 'booking';
          } else {
            currentLast = cleaningsHistory.shift();
            currentLast['type'] = 'cleaning';
          }

          toBeReturned.merged_history.unshift(currentLast);
        }

        return toBeReturned;
      }),
    );

  }

  public downloadHistory(history: any, filename) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(history[0]);
    let csv = history.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var blob = new Blob([csvArray], { type: 'text/csv' })
    saveAs(blob, `${filename}.csv`);
  }

  public getHistoryItemTransaction(hash: string) {
    return this.http.get(`/api/blockchain-interactor/blocks/hash/${hash}`);
  }

}

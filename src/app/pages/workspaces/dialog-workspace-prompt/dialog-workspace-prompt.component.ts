import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NbDialogRef, NbStepperComponent } from '@nebular/theme';
import { catchError, tap } from 'rxjs/operators';
import { Workspace } from '../models/workspace.model';
import { WorkspacesService } from '../services/workspaces.service';
import { UniqueWorkspaceNameValidator } from '../validators/unique-workspace-name.validator';
import { UniqueWorkspaceRfidValidator } from '../validators/unique-workspace-rfid.validator';

export type CreationStatus = 'creating' | 'failed' | 'successful';
@Component({
  selector: 'ngx-dialog-workspace-prompt',
  templateUrl: 'dialog-workspace-prompt.component.html',
  styleUrls: [
    'dialog-workspace-prompt.component.scss',
    'spinner.scss',
  ],
})
export class DialogWorkspacePromptComponent {

  @ViewChild('stepper') stepper: NbStepperComponent;

  public creationResult: CreationStatus = 'creating';

  public workspaceNameForm = this.fb.group({
    name: [
      '',
      [
        Validators.required,
        Validators.minLength(4),
      ],
      [
        new UniqueWorkspaceNameValidator(this.workspacesService),
      ],
    ],
  });

  public workspaceRfidForm = this.fb.group({
    rfid: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
      ],
      [
        new UniqueWorkspaceRfidValidator(this.workspacesService),
      ],
    ],
  });

  public workspace = new Workspace();

  constructor(
    protected ref: NbDialogRef<DialogWorkspacePromptComponent>,
    private readonly fb: FormBuilder,
    private readonly workspacesService: WorkspacesService,
  ) { }

  cancel() {
    this.ref.close(false);
  }

  close() {
    this.ref.close(true);
  }

  onNameSubmit() {
    this.workspaceNameForm.markAsDirty();
    this.workspace.name = this.workspaceNameForm.value.name;
    this.stepper.next();
  }

  onRfidSubmit() {
    this.workspaceRfidForm.markAsDirty();
    this.workspace.rfid = this.workspaceRfidForm.value.rfid;
    this.stepper.next();
  }

  onAddWorkspace() {
    this.creationResult = 'creating';
    return this.workspacesService.addWorkspace(this.workspace).pipe(
      tap((workspace) => this.creationResult = 'successful'),
      catchError((err, caught) => {
        console.log(err); // tslint:disable-line no-console
        this.creationResult = 'failed';
        return null;
      }),
    ).subscribe();
  }

  getCreationResult() {
    return this.creationResult.toString();
  }

}

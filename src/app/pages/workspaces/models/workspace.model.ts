export class Workspace {
    _id?: string;
    name: string;
    room: string;
    rfid: string;
    clean: boolean;
    available: boolean;
    enabled: boolean;
}

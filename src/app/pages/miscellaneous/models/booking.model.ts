export interface Booking {
  workspace: string;
  user: string;
  start_datetime: number;
  end_datetime: number;
  submission_datetime: number;
  consumed: boolean;
  checkout_datetime: number;
  cancellation_datetime: number;
}


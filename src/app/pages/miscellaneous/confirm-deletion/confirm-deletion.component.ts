import { Component, Inject } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
    selector: 'ngx-confirm-deletion',
    templateUrl: 'confirm-deletion.component.html',
    styleUrls: ['confirm-deletion.component.scss'],
})
export class ConfirmDeletionComponent {

    constructor(
        protected ref: NbDialogRef<ConfirmDeletionComponent>,
    ) { }

    cancel() {
        this.ref.close(false);
    }

    submit() {
        this.ref.close(true);
    }
}

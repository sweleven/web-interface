import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Rooms',
    icon: 'briefcase-outline',
    link: '/pages/rooms',
  },
  {
    title: 'Users',
    icon: 'people-outline',
    link: '/pages/users',
  },
  {
    title: 'Reports',
    icon: 'trending-up-outline',
    link: '/pages/reports',
  },
  {
    title: 'Verifier',
    icon: 'checkmark-square-2-outline',
    link: '/pages/verifier',
  },
  {
    title: 'Support',
    group: true,
  },
  {
    title: 'User guide',
    icon: 'question-mark-circle-outline',
    url: 'https://sweleven.gitlab.io/blockcovid/manualeUtente',
    target: '_blank',
  },
];

import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbCalendarRange } from '@nebular/theme';
import { Booking } from 'app/pages/miscellaneous/models/booking.model';
import { Cleaning } from 'app/pages/miscellaneous/models/cleaning.model';
import { Room } from 'app/pages/rooms/models/room.model';
import { User } from 'app/pages/users/models/user.model';
import { map, tap } from 'rxjs/operators';
import { ReportsService } from '../services/reports.service';

@Component({
  selector: 'ngx-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {

  public usersForm: FormGroup;
  public workspacesForm: FormGroup;
  public typeForm: FormGroup;


  public max: Date;
  public range: NbCalendarRange<Date>;

  public users: User[];
  public rooms: Room[];

  constructor(
    private readonly reportsService: ReportsService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly fb: FormBuilder,
  ) {

    this.usersForm = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    });

    this.workspacesForm = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    });

    this.typeForm = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    });

    this.max = new Date();
    this.range = {
      start: new Date(),
      end: new Date(),
    };

    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ users, rooms }) => {
          this.users = users;
          this.rooms = rooms;
        }),
      )
      .subscribe();
  }

  ngOnInit(): void {
  }

  onUsersCheckboxChange(checked, value) {
    const checkArray: FormArray = this.usersForm.get('checkArray') as FormArray;

    if (checked) {
      checkArray.push(new FormControl(value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  // onRoomsCheckboxChange(checked, room) {
  //   const checkArray: FormArray = this.workspacesForm.get('checkArray') as FormArray;

  //   if (checked) {
  //     const workspaces = room.workspaces.reduce((acc, cur) => acc.concat(cur.name), []);
  //     checkArray.push(new FormControl(workspaces));
  //   } else {
  //     const workspaces = room.workspaces.reduce((acc, cur) => acc.concat(cur.name), []);

  //     let i: number = 0;
  //     checkArray.controls.forEach((item: FormControl) => {
  //       if (workspaces.includes(item.value) !== -1) {
  //         checkArray.removeAt(i);
  //       }
  //       i++;
  //     });
  //   }

  //   console.log(this.workspacesForm.value)
  // }

  onWorkspacesCheckboxChange(checked, value) {
    const checkArray: FormArray = this.workspacesForm.get('checkArray') as FormArray;

    if (checked) {
      checkArray.push(new FormControl(value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onTypeCheckboxChange(checked, value) {
    const checkArray: FormArray = this.typeForm.get('checkArray') as FormArray;

    if (checked) {
      checkArray.push(new FormControl(value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onSubmit() {
    const workspaces = this.workspacesForm.value.checkArray.reduce((acc, cur) => acc.concat(cur._id), []);

    if (this.typeForm.value.checkArray.includes('cleanings')) {
      this.reportsService.getCleaningsReport({
        users: this.usersForm.value.checkArray,
        workspaces: this.workspacesForm.value.checkArray,
        start_date: this.range.start,
        end_date: this.range.end,
      }).pipe(
        map((report_data: Cleaning[]) => {
          return report_data ? report_data : [{}];
        }),
        map((report_data: Cleaning[]) => this.reportsService.downloadReport(report_data, 'Cleanings_report')),
      ).subscribe();
    }

    if (this.typeForm.value.checkArray.includes('bookings')) {
      this.reportsService.getBookingsReport({
        users: this.usersForm.value.checkArray,
        workspaces: workspaces,
        start_date: this.range.start,
        end_date: this.range.end,
      }).pipe(
        map((report_data: Booking[]) => {
          return report_data ? report_data : [{}];
        }),
        map((report_data: Booking[]) => this.reportsService.downloadReport(report_data, 'Bookings_report')),
      ).subscribe();
    }
  }

}

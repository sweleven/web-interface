import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports/reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { NbButtonModule, NbCalendarRangeModule, NbCardModule, NbCheckboxModule, NbDatepickerModule, NbInputModule, NbStepperModule } from '@nebular/theme';
import { ReportsService } from './services/reports.service';



@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    NbDatepickerModule,
    NbCardModule,
    NbInputModule,
    NbCalendarRangeModule,
    NbStepperModule,
    NbButtonModule,
    NbCheckboxModule,
  ],
  providers: [
    ReportsService,
  ],
})
export class ReportsModule { }

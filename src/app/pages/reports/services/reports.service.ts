import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Booking } from 'app/pages/miscellaneous/models/booking.model';
import { Cleaning } from 'app/pages/miscellaneous/models/cleaning.model';
import { Observable } from 'rxjs';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root',
})
export class ReportsService {

  constructor(
    protected readonly http: HttpClient,
  ) { }

  public getCleaningsReport(filters: any): Observable<Cleaning[]> {
    return this.http.post<Cleaning[]>(`/api/cleanings/reports`, filters);
  }

  public getBookingsReport(filters: any): Observable<Booking[]> {
    return this.http.post<Booking[]>(`/api/booking/reports`, filters);
  }

  public downloadReport(data: any, filename) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    const csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const blob = new Blob([csvArray], { type: 'text/csv' });
    saveAs(blob, `${filename}.csv`);
  }
}

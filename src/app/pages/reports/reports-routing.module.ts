import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomsListResolver } from '../rooms/rooms-list-component/resolvers/rooms-list.resolver';
import { UsersResolver } from '../users/resolvers/users.resolver';
import { ReportsComponent } from './reports/reports.component';

const routes: Routes = [{
    path: '',
    component: ReportsComponent,
    resolve: {
        users: UsersResolver,
        rooms: RoomsListResolver,
    },
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ReportsRoutingModule {

}

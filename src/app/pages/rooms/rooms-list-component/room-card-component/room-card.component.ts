import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDeletionComponent } from 'app/pages/miscellaneous/confirm-deletion/confirm-deletion.component';
import { tap } from 'rxjs/operators';
import { Occupation } from '../../models/occupation.model';
import { Room } from '../../models/room.model';

export type fillStatus = 'primary' | 'info' | 'success' | 'warning' | 'danger';

@Component({
  selector: 'ngx-room-card',
  templateUrl: './room-card.component.html',
  styleUrls: ['./room-card.component.scss'],
})
export class RoomCardComponent implements OnInit {

  @Input()
  public room: Room;

  @Output()
  onDelete = new EventEmitter<Room>();

  public workspacesUsagePercentage;
  public freeWorkspaces;
  public fillStatus;

  constructor(
    private readonly http: HttpClient,
    private readonly dialogService: NbDialogService,
  ) { }

  ngOnInit(): void {

    // Get occupation metrics from booking service
    this.http.get(`/api/booking/occupation/rooms/${this.room._id}`).pipe(
      tap((occupation: Occupation) => {
        this.workspacesUsagePercentage = Math.round(occupation.occupation * 100);
        this.freeWorkspaces = occupation.currently_free;
        this.fillStatus = this.getFillStatus();
      }),
    ).subscribe();
  }

  /**
   * Decodes this.room.fillStatus into colors
   *
   * @return {*}  {fillStatus}
   * @memberof RoomComponent
   */
  getFillStatus(): fillStatus {
    if (this.workspacesUsagePercentage > 80) {
      return 'danger';
    } else if (this.workspacesUsagePercentage > 60) {
      return 'warning';
    } else {
      return 'success';
    }
  }

  deleteRoom() {

    this.dialogService.open(ConfirmDeletionComponent)
      .onClose.subscribe(confirm => {
        // console.log(confirm);
        if (confirm) {
          this.onDelete.emit(this.room);
        }
      });

  }

}

import { Injectable } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { RoomsService } from '../../services/rooms.service';

@Injectable({ providedIn: 'root' })
export class UniqueRoomNameValidator implements AsyncValidator {
  constructor(private roomsService: RoomsService) {}

  validate(
    ctrl: AbstractControl,
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.roomsService.isRoomNameTaken(ctrl.value).pipe(
      map(isTaken => (isTaken ? { uniqueRoomName: true } : null)),
      catchError(() => of(null)),
    );
  }
}

import { Injectable } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { RoomsService } from '../../services/rooms.service';

@Injectable({ providedIn: 'root' })
export class UniqueRoomRfidValidator implements AsyncValidator {
  constructor(private roomsService: RoomsService) {}

  validate(
    ctrl: AbstractControl,
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.roomsService.isRfidTaken(ctrl.value).pipe(
      map(isTaken => (isTaken ? { uniqueRfid: true } : null)),
      catchError(() => of(null)),
    );
  }
}

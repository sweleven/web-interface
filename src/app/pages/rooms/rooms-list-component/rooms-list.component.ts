import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { timer } from 'rxjs';
import { debounce, filter, map, switchMap, tap } from 'rxjs/operators';
import { Room } from '../models/room.model';
import { RoomsService } from '../services/rooms.service';
import { DialogRoomPromptComponent } from './dialog-room-prompt/dialog-room-prompt.component';

@Component({
  selector: 'ngx-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss'],
})
export class RoomsListComponent implements OnInit {
  public rooms: Room[] = [];
  public searchForm = this.fb.group({
    name: '',
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly roomsService: RoomsService,
    private readonly fb: FormBuilder,
    private dialogService: NbDialogService,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ rooms }) => {
          this.rooms = rooms;
        }),
      )
      .subscribe();
  }

  ngOnInit(): void {

    // Update rooms array according to live search (debounce 300ms)
    this.searchForm.valueChanges.pipe(
      debounce(() => timer(300)),
      tap((values) => {
        this.onSearchRoom(values);
      }),
    ).subscribe();
  }

  onSearchRoom(values): void {
    this.roomsService.searchRoom(values).pipe(
      tap((rooms: Room[]) => {
        this.rooms = rooms;
      }),
    ).subscribe();
  }

  addRoom() {
    this.dialogService.open(DialogRoomPromptComponent)
      .onClose
      .pipe(
        filter((status: boolean) => status),
        switchMap(() => {
          return this.roomsService.getRooms();
        }),
        tap((rooms: Room[]) => {
          this.rooms = rooms;
        }),
      )
      .subscribe();
  }

  deleteRoom(room: Room) {
    this.roomsService.deleteRoom(room).pipe(
      map((_room: Room) => {
        this.rooms = this.rooms.filter(__room => __room._id !== _room._id);
      }),
    ).subscribe();
  }
}

import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NbDialogRef, NbStepperComponent } from '@nebular/theme';
import { catchError, tap } from 'rxjs/operators';
import { Room } from '../../models/room.model';
import { RoomsService } from '../../services/rooms.service';
import { UniqueRoomRfidValidator } from '../validators/unique-room-rfid.validator';
import { UniqueRoomNameValidator } from '../validators/unique-room-name.validator';

export type CreationStatus = 'creating' | 'failed' | 'successful';

@Component({
  selector: 'ngx-dialog-room-prompt',
  templateUrl: 'dialog-room-prompt.component.html',
  styleUrls: [
    'dialog-room-prompt.component.scss',
    'spinner.scss',
  ],
})
export class DialogRoomPromptComponent {

  @ViewChild('stepper') stepper: NbStepperComponent;

  public creationResult: CreationStatus = 'creating';

  public roomNameForm = this.fb.group({
    name: [
      '',
      [
        Validators.required,
        Validators.minLength(4),
      ],
      [
        new UniqueRoomNameValidator(this.roomsService),
      ],
    ],
  });

  public roomRfidForm = this.fb.group({
    rfid: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
      ],
      [
        new UniqueRoomRfidValidator(this.roomsService),
      ],
    ],
  });

  public room = new Room();

  public roomWorkspacesForm = this.fb.group({
    workspaces: '',
  });

  constructor(
    protected ref: NbDialogRef<DialogRoomPromptComponent>,
    private readonly fb: FormBuilder,
    private readonly roomsService: RoomsService,
  ) { }

  cancel() {
    this.ref.close(false);
  }

  close() {
    this.ref.close(true);
  }

  onNameSubmit() {
    this.roomNameForm.markAsDirty();
    this.room.name = this.roomNameForm.value.name;
    this.stepper.next();
  }

  onRfidSubmit() {
    this.roomRfidForm.markAsDirty();
    this.room.rfid = this.roomRfidForm.value.rfid;
    this.stepper.next();
  }

  onWorkspacesSubmit() {
    this.roomWorkspacesForm.markAsDirty();
    this.room.workspaces = this.roomWorkspacesForm.value.workspaces;
    this.stepper.next();
  }

  onAddRoom() {
    this.creationResult = 'creating';
    return this.roomsService.addRoom(this.room).pipe(
      tap((room) => this.creationResult = 'successful'),
      catchError((err, caught) => {
        console.log(err); // tslint:disable-line no-console
        this.creationResult = 'failed';
        return null;
      }),
    ).subscribe();
  }

  getCreationResult() {
    return this.creationResult.toString();
  }

}

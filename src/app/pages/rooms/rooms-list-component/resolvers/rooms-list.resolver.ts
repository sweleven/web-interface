import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Room } from '../../models/room.model';
import { RoomsService } from '../../services/rooms.service';

@Injectable()
export class RoomsListResolver implements Resolve<Room[]> {

    constructor(private roomsService: RoomsService) {

    }

    resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Room[]> | Promise<Room[]> | Room[] {
        return this.roomsService.getRooms();
    }
}

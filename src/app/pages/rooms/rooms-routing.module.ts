import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkspaceDetailsResolver } from '../workspaces/workspace-details/resolvers/workspace-details.resolver';
import { WorkspaceDetailsComponent } from '../workspaces/workspace-details/workspace-details.component';
import { RoomDetailsResolver } from './room-details/resolvers/room-details.resolver';
import { RoomDetailsComponent } from './room-details/room-details.component';
import { RoomsListResolver } from './rooms-list-component/resolvers/rooms-list.resolver';
import { RoomsListComponent } from './rooms-list-component/rooms-list.component';
import { RoomsComponent } from './rooms.component';

const routes: Routes = [{
    path: '',
    component: RoomsComponent,
    children: [
        {
            path: '',
            component: RoomsListComponent,
            resolve: {
                rooms: RoomsListResolver,
            },
        },
        {
            path: ':id',
            component: RoomDetailsComponent,
            resolve: {
                room: RoomDetailsResolver,
            },
        },
        {
            path: ':room/:id',
            component: WorkspaceDetailsComponent,
            resolve: {
                workspace: WorkspaceDetailsResolver,
            },
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RoomsRoutingModule {

}

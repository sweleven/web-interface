import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Room } from '../models/room.model';
import { UniqueRoomNameValidator } from '../rooms-list-component/validators/unique-room-name.validator';
import { UniqueRoomRfidValidator } from '../rooms-list-component/validators/unique-room-rfid.validator';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'ngx-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.scss'],
})
export class RoomDetailsComponent implements OnInit {

  public room: Room;

  public edit: boolean = false;

  public activationForm = new FormControl(true);

  public editForm = this.fb.group({
    rfid: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
      ],
      [
        new UniqueRoomRfidValidator(this.roomsService),
      ],
    ],
    name: [
      '',
      [
        Validators.required,
        Validators.minLength(4),
      ],
      [
        new UniqueRoomNameValidator(this.roomsService),
      ],
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly roomsService: RoomsService,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ room }) => {
          this.room = room;
          this.activationForm.setValue(room.enabled);
          this.activationForm.valueChanges.pipe(
            switchMap((val) => {
              return this.roomsService.updateRoom(this.room, { enabled: val });
            }),
            tap((_room: Room) => this.room = _room),
          ).subscribe();
        }),
      )
      .subscribe();
  }

  ngOnInit(): void {
  }



  public deleteRoom() {
    // this.onDelete.emit(this.room);
  }

  public editRoom() {
    this.edit = true;
    this.editForm.setValue({
      name: this.room.name,
      rfid: this.room.rfid,
    });

  }

  public saveEdits() {
    this.roomsService.updateRoom(this.room, this.editForm.value).pipe(
      tap((_room) => this.room = _room),
      tap(() => this.edit = false),
    ).subscribe();
  }

  public cancelEdits() {
    this.edit = false;
  }


}

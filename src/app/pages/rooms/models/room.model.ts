import { Workspace } from '../../workspaces/models/workspace.model';

export class Room {
    _id?: string;
    name: string;
    rfid: string;
    enabled: boolean;
    workspaces?: Workspace[];
    workspaces_total: number;
    currentlyUsedWorkspacesCount: number;
    currentlyBookedWorkspacesCount: number;
}

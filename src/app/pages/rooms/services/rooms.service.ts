import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpService } from 'app/http/http.service';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Room } from '../models/room.model';

@Injectable({
  providedIn: 'root',
})
export class RoomsService extends HttpService<Room> {

  constructor(
    protected readonly http: HttpClient,
  ) {
    super(http, 'inventory/rooms');
  }

  public getRooms(): Observable<Room[]> {
    return this.get();
  }

  public getRoom(id: string): Observable<Room> {
    return this.http.get<Room>(`/api/inventory/rooms/${id}`);
  }

  public isRoomNameTaken(roomName: string): Observable<boolean> {
    return this.searchRoom({ name: roomName }).pipe(
      map((rooms: Room[]) => rooms.length !== 0),
    );
  }

  public addRoom(room: Room): Observable<Room> {
    return this.post(room);
  }

  public searchRoom(searchParams: any): Observable<Room[]> {

    const params = new URLSearchParams(searchParams);
    return this.get(params.toString());
  }

  public isRfidTaken(rfid: string): Observable<boolean> {
    return this.http.get(`/api/inventory/rfid/${rfid}`).pipe(
      map((val) => !!val),
    );
  }

  public deleteRoom(room: Room): Observable<Room> {
    return this.delete(`/${room._id}`);
  }

  public updateRoom(room: Room, values: any): Observable<Room> {
    return this.http.patch<Room>(`/api/inventory/rooms/${room._id}`, values);
  }

}

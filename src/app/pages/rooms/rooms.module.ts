import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAlertModule, NbButtonModule, NbCardModule, NbCheckboxModule, NbDialogModule, NbIconModule } from '@nebular/theme';
import { NbInputModule, NbRadioModule, NbToggleModule, NbTooltipModule } from '@nebular/theme';
import { NbListModule, NbProgressBarModule, NbStepperModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { RoomsListResolver } from './rooms-list-component/resolvers/rooms-list.resolver';
import { RoomsRoutingModule } from './rooms-routing.module';
import { RoomsListComponent } from './rooms-list-component/rooms-list.component';
import { RoomCardComponent } from './rooms-list-component/room-card-component/room-card.component';
import { RoomsService } from './services/rooms.service';
import { RoomDetailsComponent } from './room-details/room-details.component';
import { RoomDetailsResolver } from './room-details/resolvers/room-details.resolver';
import { RoomsComponent } from './rooms.component';
import { DialogRoomPromptComponent } from './rooms-list-component/dialog-room-prompt/dialog-room-prompt.component';
import { WorkspacesListComponent } from '../workspaces/workspaces-list/workspaces-list.component';
import { WorkspaceCardComponent } from '../workspaces/workspace-card/workspace-card.component';
import { DialogWorkspacePromptComponent } from '../workspaces/dialog-workspace-prompt/dialog-workspace-prompt.component';
import { ConfirmDeletionComponent } from '../miscellaneous/confirm-deletion/confirm-deletion.component';
import { WorkspaceDetailsComponent } from '../workspaces/workspace-details/workspace-details.component';
import { WorkspaceDetailsResolver } from '../workspaces/workspace-details/resolvers/workspace-details.resolver';
import { WorkspaceHistoryDownloadDialogComponent } from '../workspaces/workspace-details/workspace-history-download-dialog/workspace-history-download-dialog.component';

@NgModule({
  declarations: [
    RoomsComponent,
    RoomsListComponent,
    RoomCardComponent,
    RoomDetailsComponent,
    DialogRoomPromptComponent,
    WorkspacesListComponent,
    WorkspaceCardComponent,
    DialogWorkspacePromptComponent,
    ConfirmDeletionComponent,
    WorkspaceDetailsComponent,
    WorkspaceHistoryDownloadDialogComponent,
  ],
  imports: [
    CommonModule,
    RoomsRoutingModule,
    ReactiveFormsModule,
    NbInputModule,
    NbCardModule,
    NbListModule,
    NbButtonModule,
    NbIconModule,
    NbProgressBarModule,
    NbDialogModule.forRoot(),
    NbStepperModule,
    NbToggleModule,
    NbDialogModule,
    NbTooltipModule,
    NbAlertModule,
    NbRadioModule,
    NbListModule,
    NbCheckboxModule,
  ],
  providers: [
    RoomsService,
    RoomsListResolver,
    RoomDetailsResolver,
    WorkspaceDetailsResolver,
  ],
  entryComponents: [
    ConfirmDeletionComponent,
    WorkspaceHistoryDownloadDialogComponent,
  ],
})
export class RoomsModule { }

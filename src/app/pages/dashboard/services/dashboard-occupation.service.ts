import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Workspace } from 'app/pages/workspaces/models/workspace.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
  })
  export class DashboardOccupationService {

    constructor(
      protected readonly http: HttpClient,
    ) { }

    public getCurrentOccupation(): Observable<any> {
        return this.http.get<Workspace[]>(`/api/inventory/workspaces`).pipe(
            map((workspaces: Workspace[]) => {
                return workspaces.reduce((acc, cur) => {
                    if (cur.available) {
                        acc.free += 1;
                    } else {
                        acc.occupied += 1;
                    }

                    return acc;
                }, { free: 0, occupied: 0 });
            }),
            map((occupation: any) => {
                // console.log(occupation);
                return [{ name: 'Free', value: occupation.free }, { name: 'Occupied', value: occupation.occupied }];
            }),
        );
    }
}

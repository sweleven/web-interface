import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DashboardOccupationService } from '../services/dashboard-occupation.service';

@Injectable()
export class DashboardOccupationResolver implements Resolve<any> {

    constructor(private dashboardOccupationService: DashboardOccupationService) { }

    resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.dashboardOccupationService.getCurrentOccupation();
    }
}

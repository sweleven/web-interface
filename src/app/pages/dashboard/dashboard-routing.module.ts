import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashboardOccupationResolver } from './resolvers/dashboard-occupation.resolver';

const routes: Routes = [{
    path: '',
    component: DashboardComponent,
    resolve: {
        occupation: DashboardOccupationResolver,
    },
}];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {

}

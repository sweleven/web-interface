import { NgModule } from '@angular/core';
import { NbCardModule } from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { D3AdvancedPieComponent } from './advanced-pie-chart/advanced-pie-chart.component';
import { DashboardComponent } from './dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DashboardOccupationResolver } from './resolvers/dashboard-occupation.resolver';
import { DashboardOccupationService } from './services/dashboard-occupation.service';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    NgxChartsModule,
    DashboardRoutingModule,
  ],
  declarations: [
    DashboardComponent,
    D3AdvancedPieComponent,
  ],
  providers: [
    DashboardOccupationResolver,
    DashboardOccupationService,
  ],
})
export class DashboardModule { }

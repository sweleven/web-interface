import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {

  public occupation = null;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
    // Load data from resolver
    this.activatedRoute.data
      .pipe(
        tap(({ occupation }) => {
          // console.log(occupation);
          this.occupation = occupation;
        }),
      )
      .subscribe();
  }
}

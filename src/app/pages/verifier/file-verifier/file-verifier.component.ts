import { Component } from '@angular/core';
import { switchMap, tap } from 'rxjs/operators';
import { VerificationResult } from '../model/verificationResult.model';
import { VerifierService } from '../verifier.service';
import {
  NgxFileDropEntry,
} from 'ngx-file-drop';
import { combineLatest, from } from 'rxjs';

@Component({
  selector: 'ngx-file-verifier',
  templateUrl: './file-verifier.component.html',
  styleUrls: ['./file-verifier.component.scss'],
})
export class FileVerifierComponent {
  public files: NgxFileDropEntry[] = [];

  public verificationResult: VerificationResult = null;

  constructor(private readonly verifierService: VerifierService) { }

  public dropped(files: NgxFileDropEntry[]) {
    // TODO: Check for duplicates
    this.files = this.files.concat(files);
    // console.log(files);
  }

  // public fileOver(event){
  //   console.log(event);
  // }

  // public fileLeave(event){
  //   console.log(event);
  // }

  // onCheckFiles(): void {
  //   this.verifierService.readFiles(this.files).subscribe(res => {
  //     console.log(res);
  //   });
  // }

  onCheckFiles(): void {

    const files_content$ = this.files.reduce((acc, cur) => {
      return acc.concat(this.verifierService.readFile(cur));
    }, []);


    combineLatest(files_content$).pipe(
      switchMap((files_content: string[]) => from(files_content)),
      switchMap((file_content: string) => this.verifierService.verifyJson(file_content)),
      tap((resp) => {
        this.verificationResult = resp;
      }),
    ).subscribe();

  }


  // const hash = this.verifierService.hashJson(this.jsonForm.value['json']);
  // this.verifierService.verifyHash(hash).pipe(
  //   tap((resp) => {
  //     this.verificationResult = resp;
  //   })
  // ).subscribe();
}

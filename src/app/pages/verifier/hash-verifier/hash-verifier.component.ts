import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { VerificationResult } from '../model/verificationResult.model';
import { VerifierService } from '../verifier.service';

@Component({
  selector: 'ngx-hash-verifier',
  templateUrl: './hash-verifier.component.html',
  styleUrls: ['./hash-verifier.component.scss'],
})
export class HashVerifierComponent {

  public hashForm = this.fb.group({
    hash: [''],
  });

  public verificationResult: VerificationResult = null;

  constructor(
    private readonly verifierService: VerifierService,
    private readonly fb: FormBuilder,
  ) { }

  onCheckHash(): void {
    const hash = this.hashForm.value['hash'];
    this.verifierService.verifyHash(hash).pipe(
      tap((resp) => {
        this.verificationResult = resp;
      }),
    ).subscribe();
  }

}

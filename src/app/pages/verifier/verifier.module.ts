import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifierComponent } from './verifier.component';
import { VerifierRoutingModule } from './verifier-routing.module';
import { QRCodeModule } from 'angular2-qrcode';
import { NbButtonModule, NbCardModule, NbInputModule, NbTabsetModule } from '@nebular/theme';
import { FileVerifierComponent } from './file-verifier/file-verifier.component';
import { HashVerifierComponent } from './hash-verifier/hash-verifier.component';
import { JsonVerifierComponent } from './json-verifier/json-verifier.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxFileDropModule } from 'ngx-file-drop';

@NgModule({
  imports: [
    CommonModule,
    VerifierRoutingModule,
    QRCodeModule,
    NbTabsetModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    ReactiveFormsModule,
    NgxFileDropModule,
  ],
  declarations: [
    VerifierComponent,
    FileVerifierComponent,
    HashVerifierComponent,
    JsonVerifierComponent,
  ],
})
export class VerifierModule { }

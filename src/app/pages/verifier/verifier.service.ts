import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpService } from 'app/http/http.service';
import { Observable } from 'rxjs';
import { Block } from './model/block.model';
import { VerificationResult } from './model/verificationResult.model';
import SHA3 from 'sha3';
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
} from 'ngx-file-drop';

@Injectable({
  providedIn: 'root',
})
export class VerifierService extends HttpService<Block> {
  constructor(protected readonly http: HttpClient) {
    super(http, 'blockchain-interactor/blocks');
  }

  verifyHash(hash: string): Observable<VerificationResult> {
    return this.http.get<VerificationResult>(
      `/api/blockchain-interactor/blocks/hash/${hash}`,
    );
  }

  verifyJson(json: string): Observable<VerificationResult> {
    const hash = this.hashJson(json);
    return this.verifyHash(hash);
  }

  hashJson(json: string): string {
    const stringJson = JSON.stringify(JSON.parse(json));
    return new SHA3().update(stringJson).digest('hex');
  }

  readFile(file: NgxFileDropEntry): Observable<string | ArrayBuffer> {

    const fileEntry = file.fileEntry as FileSystemFileEntry;

    const reader = new FileReader(); // **for reading file**



    return new Observable(observer => {
      fileEntry.file((_file: File) => {
        reader.readAsText(_file);
        reader.onload = () => {
          observer.next(reader.result);
          observer.complete();
        };
      });
    });
  }


  // Wrapper around read file !! IMPORTANT
  private _readFile(file: File): Promise<string | ArrayBuffer> {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
      temporaryFileReader.onerror = () => {
        temporaryFileReader.abort();
        reject(new DOMException('Problem parsing input file.'));
      };

      temporaryFileReader.onload = () => {
        resolve(temporaryFileReader.result);
      };
      temporaryFileReader.readAsText(file);
    });
  }
}

export interface VerificationResult {
    found: boolean;
    transactionUrl: string;
}

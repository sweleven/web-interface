import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { VerificationResult } from '../model/verificationResult.model';
import { VerifierService } from '../verifier.service';

@Component({
  selector: 'ngx-json-verifier',
  templateUrl: './json-verifier.component.html',
  styleUrls: ['./json-verifier.component.scss'],
})
export class JsonVerifierComponent {

  public inputPlaceholder = `{
      "_id": "604406c8470e58008b40c7e5",
      "name": "gjjvj",
      "__v": 0
    }`;

  public jsonForm = this.fb.group({
    json: [''],
  });

  public verificationResult: VerificationResult = null;

  constructor(
    private readonly verifierService: VerifierService,
    private readonly fb: FormBuilder,
  ) {}

  onCheckJson(): void {
    this.verifierService
      .verifyJson(this.jsonForm.value['json'])
      .pipe(
        tap((resp) => {
          this.verificationResult = resp;
        }),
      )
      .subscribe();
  }
}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { VerifierService } from './verifier.service';

describe('Service: Verifier', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifierService],
    });
  });

  it('should ...', inject([VerifierService], (service: VerifierService) => {
    expect(service).toBeTruthy();
  }));
});
